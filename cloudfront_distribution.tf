data "aws_acm_certificate" "example_certificate" {
  domain = "${var.certificate_domain}"
}

resource "aws_cloudfront_origin_access_identity" "example_origin_access" {
  comment = "access-identity-s3"
}

resource "aws_cloudfront_distribution" "example_s3_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.example_s3_bucket.bucket_regional_domain_name}"
    origin_id   = "${var.s3_origin_id}"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.example_origin_access.cloudfront_access_identity_path}"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "${var.domain} cloudfront distribution"
  default_root_object = "index.html"

  aliases = [
    "${var.domain}",
  ]

  default_cache_behavior {
    allowed_methods = [
      "DELETE",
      "GET",
      "HEAD",
      "OPTIONS",
      "PATCH",
      "POST",
      "PUT",
    ]

    cached_methods = [
      "GET",
      "HEAD",
    ]

    target_origin_id = "${var.s3_origin_id}"
    compress         = "${var.compress}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 360
    max_ttl                = 3600
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = "${data.aws_acm_certificate.example_certificate.arn}"
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}

# cloudfront invalidation iam policy
# This policy is used to invalidate cloudfront distributions
# for example: when deploying a new version
data "aws_iam_policy_document" "example_cloudfront_create_invalidation" {
  statement {
    sid = "1"

    actions = [
      "cloudfront:CreateInvalidation",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "example_cloudfront_create_invalidation" {
  name   = "cloudfront-create-invalidation"
  path   = "/"
  policy = "${data.aws_iam_policy_document.example_cloudfront_create_invalidation.json}"
}
