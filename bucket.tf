resource "aws_s3_bucket" "example_s3_bucket" {
  bucket = "${var.domain}"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags {
    Name = "${var.domain} static site"
  }
}

# This provides a full access to the s3 bucket.
# It should be attached to the iam user that will deploy the static website in the bucket
# For example, the iam user with aws tokens that will be added to your gtlab-ci env var
data "aws_iam_policy_document" "example_bucket_policy_document" {
  statement {
    sid    = "1"
    effect = "Allow"

    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}",
      "arn:aws:s3:::${var.bucket_name}/*",
    ]
  }
}

resource "aws_iam_policy" "example_bucket_iam_policy" {
  name   = "example_bucket_policy"
  path   = "/"
  policy = "${data.aws_iam_policy_document.example_bucket_policy_document.json}"
}
