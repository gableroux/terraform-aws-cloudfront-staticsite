# See https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html
provider "aws" {
  region = "${var.aws_region}"
}
