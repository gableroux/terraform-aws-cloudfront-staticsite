output "s3_website_endpoint" {
  value = "${aws_s3_bucket.example_s3_bucket.website_endpoint}"
}

output "s3_route53_domain" {
  value = "${aws_route53_record.example_cname.fqdn}"
}

output "s3_distribution_domain" {
  value = "${aws_cloudfront_distribution.example_s3_distribution.domain_name}"
}

output "s3_distribution_id" {
  value = "${aws_cloudfront_distribution.example_s3_distribution.id}"
}
