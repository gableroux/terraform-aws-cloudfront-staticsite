terraform {
  backend "s3" {
    bucket = "example-terraform-remote-state-storage"
    key    = "terraform.tfstate"
    region = "ca-central-1"
  }
}
