# terraform-aws-cloudfront-staticsite

Example project for creating a static website on AWS using cloudfront, s3 buckets and terraform.
The generated distribution will have proper http to https redirect.

![graph](./graph.png)

## Getting started

1. Read the `.tf` files and understand what's going on (search [terraform documentation](https://www.terraform.io/docs/index.html) if in doubt)
2. [Create yourself an aws account](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/)
3. [Setup the aws cli](https://docs.aws.amazon.com/polly/latest/dg/setup-aws-cli.html) (I recommend using [named profiles](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html#cli-multiple-profiles))
4. Create a s3 bucket that will hold terraform state (see commands bellow)
5. Update variables in [`./variables.tf`](./variables.tf)
6. You should replace `exmaple` everywhere with your project name
7. Run `terraform init` and `terraform apply`

### Set your aws profile

Do this every time

```bash
export AWS_PROFILE=example-profile
```

### Setup your state bucket for terraform

This is a one time, see [terraform remote state storage](https://www.terraform.io/docs/state/remote.html) for more details.

```bash
aws s3api create-bucket \
  --bucket example-terraform-remote-state-storage \
  --region ca-central-1 \
  --create-bucket-configuration LocationConstraint=ca-central-1
```

### Apply the terraform

```bash
terraform init
terraform plan
terraform apply
```

## Considerations

Creating a cloudfront distribution at aws takes ~15 minutes when this runs for the first time. Be patient ☕

## FAQ

### Why is this so complicated?

That's not so bad, that's how cloudfront distribution work, I prefer maintaining code than maintaining manual entries in a UI. Graph is kind of big, but read only what's in the squares, these are the actual resources being created. 

### How do I update the static site?

You should do this separately in site's CI configuration. Here's an example setup using [gitlab-ci](https://about.gitlab.com/product/continuous-integration/):

```yaml
deploy:
  stage: deploy
  # required env vars in gitlab-ci settings
  # AWS_SECRET_ACCESS_KEY
  # AWS_ACCESS_KEY_ID
  # CLOUDFRONT_DISTRIBUTION_ID
  # S3_BUCKET_NAME
  environment: production
  image: python:latest
  script:
    - pip install awscli
    - aws s3 rm s3://$S3_BUCKET_NAME --recursive
    - aws s3 cp ./public/ s3://$S3_BUCKET_NAME/ --recursive --exclude ".git/*" --include "*" --acl public-read
    - aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DISTRIBUTION_ID --paths "/*"
  only:
    - master
```

### Can I contribute?

Sure, send pr, open issues

### Can I see it in action?

I did not take the time to publish an example, but if you insist, I can do it, create an issue and let me know.

## Related documentation

* [Getting started with terraform](https://learn.hashicorp.com/terraform/getting-started/build)
* [terraform cloudfront_distribution](https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html)
* [How do I use CloudFront to serve a static website hosted on Amazon S3?](https://aws.amazon.com/premiumsupport/knowledge-center/cloudfront-serve-static-website/)

## Development

### Generate a graph of the plan

1. Comment content in `config.tf` (unless it's really setup)
2. Generate the graph

a. Generate a svg file:

```bash
terraform graph -draw-cycles | dot -Tsvg -o graph.svg
```

b. Generate a png file

```bash
terraform graph -draw-cycles | dot -Tpng -o graph.png
```

### Debugging

In case you get an unclear error message, try again with `TF_LOG=TRACE`

```bash
TF_LOG=TRACE terraform your_command
```

### Format the tf files

```bash
terraform fmt
```

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)
