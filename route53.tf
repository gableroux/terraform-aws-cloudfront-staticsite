data "aws_route53_zone" "example_public_zone" {
  zone_id = "${var.route53_zone_id}"
}

resource "aws_route53_record" "example_cname" {
  name    = "${var.domain}"
  type    = "CNAME"
  zone_id = "${data.aws_route53_zone.example_public_zone.zone_id}"
  ttl     = 300

  records = [
    "${aws_cloudfront_distribution.example_s3_distribution.domain_name}",
  ]
}
