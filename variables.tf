variable "aws_region" {
  # if you change this, don't forget to update config.tf value, it can't use this variable.
  default = "ca-central-1"
}

variable "s3_origin_id" {
  default = "example_s3_origin"
}

variable "certificate_domain" {
  default = "example.com"
}

variable "route53_zone_id" {
  default = "EXAMPLEZONEID"
}

variable "domain" {
  default = "test.example.com"
}

variable "bucket_name" {
  default = "example.com"
}

variable "compress" {
  default = false
}
